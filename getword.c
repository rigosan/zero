//Sergio Santana, Carroll, CS570, due 9/5/18
#include "getword.h"
int getword(char *w){
	int iochar;
	int charCount=0;
	int negPro=0;
	
	//loop to get input from stdin
	while((iochar=getchar())!=EOF){
		/*if char is  space, skip if charCount ==0 (leading space)
		and if charCount not 0 then break out and return charCounts
		*/
		if (iochar==' ')
			if(charCount==0) ;
			else break;
		/*if iochar is newline or colon, then must use ungetc
		to place it back on stream, not necessary if have collected
		no chars
		*/
		else if (iochar=='\n'||iochar==';'){
			if(charCount!=0 || (negPro==1 && charCount==0)) ungetc(iochar,stdin);
			break;	
			}
		/*if char is $ and first char, then will begin negative
		procedure, (to flip sign of charCount in the end)
		*/
		else if (iochar=='$' && charCount==0 && negPro==0) negPro=1;
		
		/*if iochar is an actual permissible char (not \n or ; or space)
		then write w byte and increment charCount and w
		*/	
		else{
			*w=iochar;
			charCount++;
			w++;
			}
	}
	//if reached EOF without any chars
	if(iochar==EOF && charCount==0 && (!negPro) ) charCount=-255;
	
	//flip sign if doing negative procedure
	if(negPro) charCount=charCount*-1;
	
	//append null terminator to print correctly in caller code
	*w='\0';
	
	return charCount;

}
